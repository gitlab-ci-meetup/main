# Main

## Cloning repository
This repository contains git submodules so you have to use `--recursive` flag in order to clone with submodules.
```bash
git clone --recursive git@gitlab.com:gitlab-ci-meetup/main.git
```
