#!/bin/sh
# switch to branch - by default we are on detached head
git fetch origin
git checkout $CI_COMMIT_REF_NAME
git reset --hard origin/$CI_COMMIT_REF_NAME
git submodule update

# update submodules - checkout branch with the same name in submodules
git submodule foreach 'git fetch origin'
git submodule foreach 'git reset --hard origin/$CI_COMMIT_REF_NAME'

nothingToCommit=$(git diff --exit-code && echo "no changes")

if [ "$nothingToCommit" != "no changes" ]
then
    UPDATED_SUBMODULES=$(echo `git diff | grep '^+++ b/projects/' | cut -d"/" -f3` | sed 's/ / \&\& /')
    git add projects/*

    # commit and push!
    git commit -m "[pipeline]: Automatically updated pointers (${UPDATED_SUBMODULES})"
    git push origin $CI_COMMIT_REF_NAME

    # for example we can send now information to slack
#    curl -X POST --data-urlencode "payload={\"text\": \"Submodules updated (${UPDATED_SUBMODULES}) successfully on ${CI_COMMIT_REF_NAME}.\"}" $CI_SLACK_WEB_HOOK_URL

    # stop current pipeline
    # because new pipeline will run with corect pointers
    curl --request POST --header "PRIVATE-TOKEN: ${CI_PRIVATE_TOKEN}"  "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/cancel"
fi
